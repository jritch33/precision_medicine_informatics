from fastapi import FastAPI
from services.pizza_service import PizzaService
from services.ontology_service import OntologyService
from pydantic import BaseModel
from owlready2 import *

class Pizza(BaseModel):
    clazz: str
    name: str
    toppings: list

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.post("/pizza")
def pizza(pizza: Pizza):
    ontology_service = OntologyService('pizza')
    pizza_service = PizzaService(ontology_service)
    result = pizza_service.classify_pizza(pizza)
    print(result)

    return result